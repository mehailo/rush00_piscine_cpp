//
// Created by Mykhailo Frankevich on 11/4/17.
//

#ifndef RUSH00_CPP_IOBJECT_H
#define RUSH00_CPP_IOBJECT_H

class IObject {
public:
    IObject();
    IObject(float x_speed, float y_speed, char symbol, int hp);
    IObject(float x_coord, float y_coord, float x_speed, float y_speed, char symbol, int hp);
    virtual ~IObject();
    IObject& operator=(const IObject &rhs);
    IObject(const IObject &rhs);


    int getHp() const;
    void setHp(int hp);
    void TakeDamage(int damage);
    bool IsAlive();

    void Reincarnate();
    static int Random(int floor, int ceiling);

    float getX_coord() const;

    void setX_coord(float x_coord);

    float getY_coord() const;

    void setY_coord(float y_coord);
    float getX_speed() const;

    void setX_speed(float x_speed);

    float getY_speed() const;

    void setY_speed(float y_speed);

    char getSymbol() const;

    void setSymbol(char symbol);

private:
    float x_coord;
    float y_coord;
    float x_speed;
    float y_speed;
    char symbol;
    int hp;


};


#endif //RUSH00_CPP_IOBJECT_H