//
// Created by Mykhel Frankevych on 11/5/17.
//

#ifndef RUSH00_BULLET_H
#define RUSH00_BULLET_H


#include "IObject.hpp"

class Bullet : public IObject
{
public:
	Bullet(float x, float y);
};


#endif //RUSH00_BULLET_H
