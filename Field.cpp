//
// Created by Mykhailo Frankevich on 11/4/17.
//
#include <random>
#include "ncurses.h"

#include "Field.h"

Player &Field::getPlayer(){
    return player;
}

void Field::setPlayer(const Player &player)
{
}

void Field::SpawnRandomEnemies(int count)
{
    while(count--)
    {
        for (int i = 0; i < ENEMIES_COUNT; ++i)
        {
            if (enemies[i] == NULL)
            {
                if (i % 2 == 0)
                    enemies[i] = _SpawnEnemyOne();
                else
                    enemies[i] = _SpawnEnemyTwo();
                break ;
            }
            if (!enemies[i]->IsAlive())
            {
                enemies[i]->Reincarnate();
                enemies[i]->setHp(100);
                break;
            }
        }
    }
}

void Field::SpawnAsteroids(int count)
{
	while(count--)
	{
		for (int i = 0; i < ASTEROIDS_COUNT; ++i)
		{
			if (asteroids[i] == NULL)
			{
				asteroids[i] = new Asteroid(i % 2 == 0);
				asteroids[i]->Reincarnate();
				break ;
			}
			if (!asteroids[i]->IsAlive())
			{
				asteroids[i]->Reincarnate();
				asteroids[i]->setHp(100);
				break;
			}
		}
	}
}

void Field::PrintScreen() {
//    for (int y = g_y_min_border; y < g_y_max_border; ++y)
//    {
//        for (int x = g_x_min_border; x < g_x_max_border; ++x)
//            mvprintw(y, x, " ");
//    }

    //Print player
    mvprintw(player.getY_coord(), player.getX_coord(), "%c", player.getSymbol());

    //Print enemies
    for (int i = 0; i < ENEMIES_COUNT; ++i)
        if (enemies[i] && enemies[i]->IsAlive())
            mvprintw(enemies[i]->getY_coord(),
                     enemies[i]->getX_coord(),
                     "%c", enemies[i]->getSymbol());

	//Print asteroids
	for (int i = 0; i < ASTEROIDS_COUNT; ++i)
		if (asteroids[i] && asteroids[i]->IsAlive())
			mvprintw(asteroids[i]->getY_coord(),
					 asteroids[i]->getX_coord(),
					 "%c", asteroids[i]->getSymbol());

	//Print bullets
	for (int i = 0; i < BULLETS_COUNT; ++i)
		if (bullets[i] && bullets[i]->IsAlive())
			mvprintw(bullets[i]->getY_coord(),
					 bullets[i]->getX_coord(),
					 "%c", bullets[i]->getSymbol());

	_PrintInfo();
}

void Field::StaticMove()
{
    for (int i = 0; i < ENEMIES_COUNT; ++i)
        if (enemies[i] && enemies[i]->IsAlive())
            enemies[i]->setX_coord(enemies[i]->getX_coord() - enemies[i]->getX_speed() * SPEED_COEF);

	for (int i = 0; i < ASTEROIDS_COUNT; ++i)
		if (asteroids[i] && asteroids[i]->IsAlive())
		{
			asteroids[i]->setX_coord(asteroids[i]->getX_coord() - asteroids[i]->getX_speed() * SPEED_COEF);
			asteroids[i]->setY_coord(asteroids[i]->getY_coord() - asteroids[i]->getY_speed() * SPEED_COEF);
		}


	for (int i = 0; i < BULLETS_COUNT; ++i)
	{
		if (bullets[i] && bullets[i]->IsAlive())
			bullets[i]->setX_coord(bullets[i]->getX_coord() - bullets[i]->getX_speed() * SPEED_COEF);
		if (bullets[i] && bullets[i]->getX_coord() > g_x_max_border)
			bullets[i]->setHp(0);
	}

    _CheckCollisions();
}

void Field::ShootBullet()
{
	for (int i = 0; i < BULLETS_COUNT; ++i)
	{
		if (bullets[i] == NULL)
		{
			bullets[i] = new Bullet(player.getX_coord(), player.getY_coord());
			break ;
		}
		if (!bullets[i]->IsAlive())
		{
			bullets[i]->setHp(50);
			bullets[i]->setX_coord(player.getX_coord());
			bullets[i]->setY_coord(player.getY_coord());
			break;
		}
	}
}

Field::Field() : player(), score(0)
{
    for (int i = 0; i < ASTEROIDS_COUNT; ++i)
        asteroids[i] = NULL;

    for (int i = 0; i < ENEMIES_COUNT; ++i)
        enemies[i] = NULL;

	for (int i = 0; i < BULLETS_COUNT; ++i)
		bullets[i] = NULL;
}

Enemy *Field::_SpawnEnemyOne()
{
    Enemy *res;
    res = new Enemy(0.09f, 1, '<', 100);
    res->Reincarnate();
    return res;
}

Enemy *Field::_SpawnEnemyTwo()
{
    Enemy *res;
    res = new Enemy(0.07f, 1, '@', 100);
    res->Reincarnate();
    return res;
}

void Field::_CheckCollisions()
{
    IObject* objects[OBJECTS_COUNT];

    for (int i = 0; i < BULLETS_COUNT; ++i)
        objects[i] = bullets[i];

    for (int i = 0; i < ENEMIES_COUNT; ++i)
        objects[i + BULLETS_COUNT] = enemies[i];

    for (int i = 0; i < ASTEROIDS_COUNT; ++i)
        objects[i + BULLETS_COUNT + ENEMIES_COUNT] = asteroids[i];


    for (int i = 0; i < OBJECTS_COUNT; ++i)
    {
        for (int j = 0; j < OBJECTS_COUNT; ++j)
        {
            if (i != j &&
                    objects[i] && objects[i]->IsAlive() &&
                    objects[j] && objects[j]->IsAlive() &&
                    static_cast<int>(objects[i]->getX_coord()) == static_cast<int>(objects[j]->getX_coord()) &&
					static_cast<int>(objects[i]->getY_coord()) == static_cast<int>(objects[j]->getY_coord()))
            {
                objects[j]->TakeDamage(objects[i]->getHp());
				score += objects[i]->getHp();
                objects[i]->TakeDamage(100000);
            }
        }
        if (i > BULLETS_COUNT &&
                objects[i] &&
				objects[i]->IsAlive() &&
				static_cast<int>(player.getX_coord()) == static_cast<int>(objects[i]->getX_coord()) &&
				static_cast<int>(player.getY_coord()) == static_cast<int>(objects[i]->getY_coord()))
        {
			player.TakeDamage(objects[i]->getHp());
			score += objects[i]->getHp();
            objects[i]->TakeDamage(100000);
        }
    }
}

void Field::_PrintInfo()
{
    mvprintw(g_y_max_border + 2, 20, "Hit points : %5d", player.getHp());
	mvprintw(g_y_max_border + 2, g_x_max_border - 40, "Score : %06d", score);
}
