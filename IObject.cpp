//
// Created by Mykhailo Frankevich on 11/4/17.
//

#include "IObject.hpp"

#include <random>

extern int g_y_min_border;
extern int g_x_min_border;
extern int g_x_max_border;
extern int g_y_max_border;

void IObject::setX_coord(float x_coord)
{
    IObject::x_coord = x_coord;
    if ((x_coord < g_x_min_border || x_coord > g_x_max_border) && x_speed > 0)
        Reincarnate();
}

float IObject::getY_coord() const {
    return y_coord;
}

void IObject::setY_coord(float y_coord)
{
    IObject::y_coord = y_coord;
    if (y_coord < g_y_min_border || y_coord > g_y_max_border)
        Reincarnate();
}

float IObject::getX_speed() const {
    return x_speed;
}

void IObject::setX_speed(float x_speed) {
    IObject::x_speed = x_speed;
}

float IObject::getY_speed() const {
    return y_speed;
}

IObject::IObject() : x_coord(0), y_coord(0), x_speed(0), y_speed(0), symbol(' ')
{

}

IObject::~IObject() {

}

IObject& IObject::operator=(const IObject &rhs)
{
    x_coord = rhs.x_coord;
    y_coord = rhs.y_coord;

    x_speed = rhs.x_speed;
    y_speed = rhs.y_speed;

    symbol = rhs.symbol;

    return *this;
}

IObject::IObject(const IObject &rhs) {
    *this = rhs;
}

void IObject::setY_speed(float y_speed) {
    IObject::y_speed = y_speed;
}


float IObject::getX_coord() const {
    return x_coord;
}

void IObject::Reincarnate()
{
    setY_coord(IObject::Random(g_y_min_border, g_y_max_border));
    setX_coord(g_x_max_border);
}


IObject::IObject(float x_speed, float y_speed, char symbol, int hp)
		: x_speed(x_speed), y_speed(y_speed), symbol(symbol), hp(hp)
{
	setY_coord(IObject::Random(g_y_min_border, g_y_max_border));
	setX_coord(g_x_max_border);
}

int IObject::Random(int floor, int ceiling) {
    int randNum = rand()%(ceiling - floor + 1) + floor;
    randNum = randNum % (ceiling - floor) + floor;

    return (randNum);
}

int IObject::getHp() const {
    return hp;
}

void IObject::setHp(int hp) {
    IObject::hp = hp;
}

IObject::IObject(float x_coord, float y_coord, float x_speed, float y_speed, char symbol, int hp) :
		x_coord(x_coord), y_coord(y_coord), x_speed(x_speed), y_speed(y_speed), symbol(symbol), hp(hp)
{

}

void IObject::TakeDamage(int damage)
{
    hp -= damage;
	if (hp < 0)
		hp = 0;
}

bool IObject::IsAlive()
{
    return hp > 0;
}

char IObject::getSymbol() const {
    return symbol;
}

void IObject::setSymbol(char symbol) {
    IObject::symbol = symbol;
}
