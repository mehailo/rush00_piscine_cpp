//
// Created by Mykhailo Frankevich on 11/4/17.
//

#include "BaseSpaceShip.h"

#ifndef RUSH00_CPP_PLAYER_H
#define RUSH00_CPP_PLAYER_H

extern int g_y_min_border;
extern int g_x_min_border;
extern int g_x_max_border;
extern int g_y_max_border;

class Player : public BaseSpaceShip
{
public:
    Player();
    virtual ~Player();
    Player(const Player &rhs);
    Player& operator=(Player &rhs);


    void MoveUp();
    void MoveDown();
    void MoveLeft();



    void MoveRight();

};


#endif //RUSH00_CPP_PLAYER_H





//------------------------------------------------------
//
// Created by Anastasiia Trepyton on 5/12/17.
//

//#include "ncurses.h"
//#include "Screen.hpp"
//
//void print_frame(int width, int height)
//{
//    for (int x = 0; x < width; x++)
//    {
//        for (int y = 0; y < height; y++)
//        {
//            if (y == 0)
//                mvprintw(y, x, "_");
//            if ((x == 0 || x == width - 1) && y != 0)
//                mvprintw(y, x, "|");
//            if ((y == height - 2 || y == height - 1) && x != 0 )
//                mvprintw(y, x, "_");
//        }
//    }
//}

//int main()
//{
//    int frame = 0;
//    srand(time(NULL));
//    Screen game(2, 2);
//    int key;
//    WINDOW *screen;
//    int exit = 0;
//
//    initscr();
//    clear();
//    noecho();
//    cbreak();
//
//    screen = newwin(Screen::width + 3, Screen::height + 4, 0, 0);
//    keypad(screen, TRUE);
//    wtimeout(screen, 1);
//    start_color();
//    init_pair(1, COLOR_GREEN, 0);
//    init_pair(2, COLOR_BLUE, 0);
//    init_pair(3, COLOR_CYAN, 0);
//    init_pair(4, COLOR_MAGENTA, 0);
//    init_pair(5, COLOR_RED, COLOR_WHITE);
//    attron(COLOR_PAIR(2));
//    print_frame(Screen::height + 4, Screen::width + 3);
//    while (true)
//    {
//        key = wgetch(screen);
//        switch (key)
//        {
//            case KEY_UP:
//                game.player.moveX(-1);
//                break;
//            case ' ':
//                game.addProjectile(-2, game.player.get_x_position(), game.player.get_y_position() + 1);
//                break;
//            case KEY_DOWN:
//                game.player.moveX(1);
//                break;
//            case KEY_LEFT:
//                game.player.moveY(-1);
//                break;
//            case KEY_RIGHT:
//                game.player.moveY(1);
//                break;
//            case 27:
//                exit = 1;
//                break;
//            default:
//                break;
//        }
//        game.staticMove();
//        game.printScreen();
//        refresh();
//        if (game.player.get_hp() <= 0)
//        {
//            for(;;)
//            {
//                attron(COLOR_PAIR(5));
//                mvprintw(25, 100, "GAME OVER");
//                refresh();
//                key = wgetch(screen);
//                if (key == 27)
//                    return 0;
//            }
//        }
//        if (exit)
//            break;
//        if(frame % 200 == 0)
//        {
//            game.addAsteroid(1);
//            game.addSpaceInvader(2);
//        }
//        frame++;
//    }
//    clrtoeol();
//    endwin();
//    return 0;
//}

