//
// Created by Mykhailo Frankevich on 11/4/17.
//

#include "IObject.hpp"
#ifndef RUSH00_CPP_BASESPACESHIP_H
#define RUSH00_CPP_BASESPACESHIP_H


class BaseSpaceShip :public IObject
{
public:
    BaseSpaceShip();
    BaseSpaceShip(float x_speed, float y_speed, char symbol, int hp);
    BaseSpaceShip(const BaseSpaceShip& rhs);
    BaseSpaceShip& operator=(BaseSpaceShip rhs);
    virtual ~BaseSpaceShip();

};



#endif //RUSH00_CPP_BASESPACESHIP_H
