//
// Created by Mykhailo Frankevich on 11/4/17.
//

#ifndef RUSH00_CPP_FIELD_H
#define RUSH00_CPP_FIELD_H

#define ENEMIES_COUNT 5
#define ASTEROIDS_COUNT 10
#define BULLETS_COUNT 7

#define OBJECTS_COUNT (ENEMIES_COUNT + ASTEROIDS_COUNT + BULLETS_COUNT)

#define SPEED_COEF 5

#include "Player.h"
#include "Asteroid.h"
#include "Enemy.h"
#include "Bullet.h"

class Field {
public:
    void SpawnRandomEnemies(int count);
    void SpawnAsteroids(int count);
    void PrintScreen();

    Field();

    void StaticMove();
    void ShootBullet();

    Player &getPlayer();
    void setPlayer(const Player &player);

private:
    Enemy* _SpawnEnemyOne();
    Enemy* _SpawnEnemyTwo();
    void _CheckCollisions();
    void _PrintInfo();

    Player player;
    Asteroid* asteroids[ASTEROIDS_COUNT];
    Enemy* enemies[ENEMIES_COUNT];
	Bullet* bullets[BULLETS_COUNT];
	long long score;


};


#endif //RUSH00_CPP_FIELD_H
