//
// Created by Mykhailo Frankevich on 11/4/17.
//

#include "Player.h"

Player::Player() : BaseSpaceShip()
{
    setX_coord(g_x_min_border + g_y_max_border / 10);
    setY_coord(g_y_min_border + g_y_max_border / 2);
    setX_speed(1);
    setY_speed(1);
    setHp(200);
    setSymbol('>');
}

Player::~Player() {

}

void Player::MoveUp()
{
    int tmp = getY_coord();
    if (tmp <= g_y_min_border)
        return ;
    else
        setY_coord(tmp - getY_speed());
}

void Player::MoveDown()
{
    int tmp = getY_coord();
    if (tmp >= g_y_max_border)
        return ;
    else
        setY_coord(tmp + getY_speed());
}

void Player::MoveLeft()
{
    int tmp = getX_coord();
    if (tmp <= g_x_min_border)
        return ;
    else
        setX_coord(tmp - getX_speed());
}

Player &Player::operator=(Player &rhs) {
    (*this).BaseSpaceShip::operator=(rhs);
    return *this;
}

void Player::MoveRight()
{
    int tmp = getX_coord();
    if (tmp >= g_x_max_border)
        return ;
    else
        setX_coord(tmp + getX_speed());
}

Player::Player(const Player &rhs) : BaseSpaceShip(rhs)
{
    return ;
}
