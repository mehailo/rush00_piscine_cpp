#include "Player.h"
#include "Field.h"


#include "ncurses.h"

#include <iostream>
#include <zconf.h>

int g_y_min_border = 2;
int g_x_min_border = 2;
int g_x_max_border = 122;
int g_y_max_border = 37;

void PrintFrame()
{
    for (int y = g_y_min_border; y < g_y_max_border + 2; ++y)
    {
        mvprintw(y, g_x_min_border - 1, "|");
        mvprintw(y, g_x_max_border, "|");
    }

    for (int x = g_x_min_border; x < g_x_max_border; ++x)
    {
        mvprintw(g_y_min_border - 1, x, "_");
        mvprintw(g_y_max_border + 1, x, "_");
    }
}

void InitNcurses(WINDOW *screen)
{
    if (screen)
        ;
    start_color();
    init_pair(1, 0, 0);
    init_pair(2, COLOR_CYAN, 0);
    init_pair(5, COLOR_RED, COLOR_BLACK);
    attron(COLOR_PAIR(2));
}

int main()
{
    WINDOW *screen = NULL;
    Field field;

    srand(time(NULL));

    initscr();
    clear();
    noecho();
    cbreak();
    curs_set(0);
    screen = newwin(g_x_max_border + g_x_min_border, g_y_max_border + g_y_min_border + 10, 0, 0);
    wtimeout(screen, 10);

    InitNcurses(screen);
    keypad(screen, TRUE);

    long long frame = 0;
    int stop_game = 0;
    int key;

    for(;;)
    {
        key = wgetch(screen);
        switch (key) {
            case KEY_UP:
                field.getPlayer().MoveUp();
                break;
            case ' ':
                field.ShootBullet();
                break;
            case KEY_DOWN:
                field.getPlayer().MoveDown();
                break;
            case KEY_LEFT:
                field.getPlayer().MoveLeft();
                break;
            case KEY_RIGHT:
                field.getPlayer().MoveRight();
                break;
            case 27:
                stop_game = 1;
                break;
            default:
                break;
        }
        field.StaticMove();
        erase();
        field.PrintScreen();
        PrintFrame();
        refresh();
        if (!field.getPlayer().IsAlive())
        {
            for(;;)
            {
                attron(COLOR_PAIR(5));
                mvprintw((g_y_max_border - g_y_min_border) / 2, (g_x_max_border - g_x_min_border) / 2, "GAME OVER");
                refresh();
                key = wgetch(screen);
                if (key == 27)
                    return 0;
            }
        }
        if (stop_game)
            break;
        if(frame % 150 == 0)
            field.SpawnRandomEnemies(1);
        if(frame % 250 == 0)
            field.SpawnAsteroids(1);
        frame++;
    }
    clrtoeol();
    endwin();
    return 0;
}
