//
// Created by Mykhailo Frankevich on 11/4/17.
//

#include "BaseSpaceShip.h"


BaseSpaceShip::BaseSpaceShip(float x_speed, float y_speed, char symbol, int hp)
        : IObject(x_speed, y_speed, symbol, hp)
{
    return ;
}

BaseSpaceShip::BaseSpaceShip(const BaseSpaceShip &rhs)
{
    *this = rhs;
}

BaseSpaceShip &BaseSpaceShip::operator=(BaseSpaceShip rhs)
{
    (*this).IObject::operator=(rhs);
    return *this;
}

BaseSpaceShip::BaseSpaceShip(): IObject()
{

}

BaseSpaceShip::~BaseSpaceShip()
{

}
