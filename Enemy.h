//
// Created by Roman Ishchenko on 11/4/17.
//
#include "IObject.hpp"
#include "BaseSpaceShip.h"

#ifndef RUSH00_CPP_ENEMY_H
#define RUSH00_CPP_ENEMY_H

class Enemy : public BaseSpaceShip {
public:
    Enemy();
    Enemy(float x_speed, float y_speed, char symbol, int hp);

    Enemy& operator=(Enemy rhs);
    Enemy(const Enemy &rhs);

    virtual ~Enemy();
};


#endif //RUSH_ENEMY_H
