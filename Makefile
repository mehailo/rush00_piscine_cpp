#CONFIG#
NAME = ft_retro

INCLUDES =

#COMPILER#
CXX = clang++
CXXFLAGS = -Wall -Wextra -Werror -lncurses $(INCLUDES)

LIBS =

#FILES#

PATH_SRC = ./
PATH_OBJ = ./
PATH_INC = ./

SRC = IObject.cpp BaseSpaceShip.cpp Player.cpp Field.cpp main.cpp

OBJ = $(addprefix $(PATH_SRC), $(SRC:.cpp=.o))

#RULES#

.PHONY: clean fclean

all: $(NAME)

$(NAME): $(OBJ)
	$(CXX) $(OBJ) -o $(NAME) $(LIBS)

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all