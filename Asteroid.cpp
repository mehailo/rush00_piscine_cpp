//
// Created by Roman Ishchenko on 11/4/17.
//

#include "Asteroid.h"

Asteroid::Asteroid() : IObject(0.08f, 0, 'O', 75)
{
    return ;
}

Asteroid::Asteroid(bool moving_up) : IObject(0.08f, 0.005f, 'O', 75)
{
	if (!moving_up)
		setY_speed(-getY_speed());
}

Asteroid::Asteroid(float x, float y) : IObject(0.08f, 0.01f, 'O', 75)
{
    return ;
}

Asteroid::Asteroid(int x_coord, int y_coord, int x_speed, int y_speed, char symbol, int hp)
        : IObject(x_coord, y_coord, x_speed, y_speed, symbol, hp)
{
    return ;
}

Asteroid::Asteroid(const Asteroid &rhs)
{
    *this = rhs;
}


Asteroid::~Asteroid()
{
    return ;
}

Asteroid &Asteroid::operator=(Asteroid rhs) {
    (*this).IObject::operator=(rhs);
    return (*this);
}


