//
// Created by Roman Ishchenko on 11/4/17.
//

#include "IObject.hpp"
#ifndef RUSH00_CPP_ASTEROID_H
#define RUSH00_CPP_ASTEROID_H


class Asteroid : public IObject
{
public:
    Asteroid();
	Asteroid(bool moving_up);
	Asteroid(float x, float y);
    Asteroid(int x_coord, int y_coord, int x_speed, int y_speed, char symbol, int hp);
    Asteroid(const Asteroid& rhs);
    Asteroid& operator=(Asteroid rhs);
    virtual ~Asteroid();

};

#endif //RUSH_ASTEROID_H

